#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

python "src/bom_gen.py" "$DIR"

for file in $(find "$DIR/" -type f -name "*.asm"); do
    filename=$(basename $file)
    dest=$(echo $filename | sed "s/asm/pug/g")
    python "$DIR/program_gen.py" "$DIR/$filename" "$DIR/_$dest"
done
