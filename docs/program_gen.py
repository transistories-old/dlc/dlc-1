#!/usr/bin/env python3

import os
import sys
import re

INDENT = "  "
INSTRUCTIONS = [
    'NOP',
    'JMP',
    'JPO',
    'JPE',
    'JPZ',
    'LDA',
    'LDB',
    'SWP',
    'OUT',
    'INV',
    'ADD',
    'SUB',
    'ADB',
    'SBB',
    'RST'
]

INSTRUCTION_REGEX = "(" + "|".join(INSTRUCTIONS) + ")"

if len(sys.argv) == 3:
    source_file = sys.argv[1]
    dest_file = sys.argv[2]
else:
    raise AttributeError("Wrong number of arguments...")

source_file_name = os.path.basename(source_file)

# Add the root container
pug = ".snippet-container\n"
# Add the filename as a label
pug += (INDENT * 1) + ".filename\n" + (INDENT * 2) + f"p {source_file_name}\n"
# Add the pre and code tags
pug += (INDENT * 1) + "pre\n" + (INDENT * 2) + "code\n"

# Read the lines from the source file
with open(source_file, 'r') as source:
    lines = [line.rstrip() for line in source]

# Add syntax highlighting
for line in lines:
    pug += (INDENT * 3) + "| "
    # Labels
    line = re.sub(r'^(.[a-z]+)$', r'<span class="asm-label">\1</span>', line)
    # Comments
    line = re.sub(r'(;.*)', r'<span class="asm-comment">\1</span>', line)
    # Constants
    line = re.sub(r'( #0[x][0-9a-f]+)', r'<span class="asm-constant">\1</span>', line)
    # Keywords
    line = re.sub(INSTRUCTION_REGEX, r'<span class="asm-keyword">\1</span>', line)
    pug += line
    pug+= "\n"

# Write the result to the destination file
with open(dest_file, 'w') as destination:
    destination.write(pug)