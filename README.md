---
title: DLC-1
subtitle: The discrete logic computer
---

# DLC-1

## Introduction

## Construction

### Parts

#### Clock Generator & Reset Circuitry (CLK)

##### Parts

- NE555
- CD74HC08E (1/4)
- CD74HC14E
- CD74HC32E (1/4)
- CD74HC74E (x2)
- CD74HC153E
- CD74HC161E
- 100kR resistor
- 2.2kR resistor
- 1MR resistor
- 100uF capacitor (x2)
- 10pf capacitor
- 100pf capacitor (x8)

#### Program Counter (PC)

##### Parts

- CD74HC04E
- CD74HC125E
- CD74HC161E

#### Memory Address Register (MAR)

##### Parts

- CD74HC08E (1/4)
- CD74HC175E

#### Program Memory (ROM)

##### Parts

- AT28C64B
- CD74HC125E

#### Instruction Register (INS)

##### Parts

- CD74HC08E (1/4)
- CD74HC175E

#### Micro Code Counter (MCC)

##### Parts

- CD74HC161E

#### Control Unit (CU)

##### Parts

- AT28C64B (x2)

#### Arithmetic Logic Unit (ALU)

##### Parts

- 

#### Accumulator (ACC)

##### Parts

- CD74HC125E
- CD74HC175E

#### Backup Register (BAC)

##### Parts

- CD74HC173E

#### Temporary register (TMP)

##### Parts

- CD74HC175E

#### Output Register (OUT)

##### Parts

- CD74HC175E

### Schematic

### Logisim simulation

## Instruction Set

| HEX  | BIN  | INS  | Description                                               |
| ---- | ---- | ---- | --------------------------------------------------------- |
| 0x0  | 0000 | NOP  | No operation; go to the next address                      |
| 0x1  | 0001 | JMP  | Unconditional jump; 'to' specified by next address        |
| 0x2  | 0010 | JPO  | Jump on overflow                                          |
| 0x3  | 0011 | JPE  | Jump on equal operands                                    |
| 0x4  | 0100 | JPZ  | Jump on 'zero' result                                     |
| 0x5  | 0101 |      | [Not yet filled]                                          |
| 0x6  | 0110 | LDA  | Load ACC with the value, specified by the next address    |
| 0x7  | 0111 | LDB  | Load BAC with the value, specified by the next address    |
| 0x8  | 1000 | SWP  | Swap the values in ACC and BAC                            |
| 0x9  | 1001 | OUT  | Output the value from ACC                                 |
| 0xa  | 1010 | INV  | Invert the value in ACC (bitwise)                         |
| 0xb  | 1011 | ADD  | Add the value, specified by the next address, to ACC      |
| 0xc  | 1100 | SUB  | Subract the value, specified by the next addres, from ACC |
| 0xd  | 1101 | ADB  | Add the value in BAC to ACC                               |
| 0xe  | 1110 | SBB  | Subtract the value in BAC from ACC                        |
| 0xf  | 1111 | RST  | System reset                                              |

## Microcodes

## Programs

### Counter

```armasm
.start
	LDA #0x0	; Load ACC with the value at address 15
.loop
	OUT			; Ouput the ACC
	ADD #0x1	; Add the value at address to the ACC
	JPO start	; Jump on overflow to address 0 (restart)
	JMP loop	; Jump to address 1 (loop)
```

### Fibonacci

```armasm
.start
    LDA #0x1    ; Load 1 into the ACC register
    LDB #0x1    ; Load 1 into the BAC register
    OUT         ; Output the value of the ACC
.loop
    ADB         ; Add the SWP value to the ACC value
    JPO start   ; Jump to start on overflow (reset)
    OUT         ; Output the value of the ACC
    SWP         ; Swap the ACC and SWP register
    JMP loop    ; Jump back to loop
```

## Bill Of Materials

| Code             | Manufacturer      | Description                                               | Amount | Price | subtotal |
| ---------------- | ----------------- | --------------------------------------------------------- | ------ | ----- | -------- |
| K100J15C0GF53L2  | Vishay            | 10pF ceramic capacitor                                    | 5      | 0.28  |          |
| K101J15C0GF53L2  | Vishay            | 100pF ceramic capacitor                                   | 8      | 0.102 |          |
| 860020772005     | Wurth Elektronik  | 1uF electrolytic capacitor                                | 2      | 0.093 |          |
| 860080772001     | Wurth Elektronik  | 10uF electrolytic capacitor                               | 1      | 0.102 |          |
| 1N4448           |                   | Diode                                                     | 5      | 0.085 |          |
| 4609H-101-103LF  | Bourns            | 10kR bussed resistor (x8)                                 | 1      | 0.508 |          |
| CFR-25JB-52-100R | Yageo             | 100R resistor                                             | 4      | 0.085 |          |
| CFR-25JB-52-10K  | Yageo             | 10kR resistor                                             | 5      | 0.085 |          |
| CFR-25JB-52-100K | Yageo             | 100kR resistor                                            | 1      | 0.085 |          |
| B3F-4050         | Omron Electronics | 12x12mm tactile switch                                    | 5      | 0.373 |          |
| B32-1360         | Omron Electronics | Tactile switch cap (white)                                | 5      | 0.246 |          |
| NE555P           | Texas Intruments  | 555 timer-ic                                              | 1      | 0.39  |          |
| CD74HC08E        | Texas Intruments  | Quad 2-input AND gate                                     | 1      | 0.491 |          |
| CD74HC14E        | Texas Intruments  | Hex Schmitt trigger, inverter                             | 1      | 0.491 |          |
| CD74HC32E        | Texas Intruments  | Quad 2-input OR gate                                      | 1      | 0.50  |          |
| CD74HC74E        | Texas Intruments  | Dual D Flip-Flop with Set and Reset Positive-Edge Trigger | 2      | 0.508 |          |
| CD74HC153E       | Texas Intruments  | Dual 4:1 decoder                                          | 1      | 0.728 |          |
| CD74HC161E       | Texas Intruments  | 4bit counter, presetable, async reset                     | 1      | 0.584 |          |

### Temporary...

| Code          | Manufacturer         | Description                      | Amount | Price | Subtotal |
| ------------- | -------------------- | -------------------------------- | ------ | ----- | -------- |
| TLHR5400      | Vishay               | 5mm diffused led, red            |        |       |          |
| TLHY5400      | Vishay               | 5mm diffused led, yellow         |        |       |          |
| TLHG5400      | Vishay               | 5mm diffused led, green          |        |       |          |
| TLHB5400      | Vishay               | 5mm diffused led, blue           |        |       |          |
| LTL-307A      | Lite-On              | 5mm diffused led, amber          |        |       |          |
| AT28C64B-15PU | Microchip Technology | EEPROM 8k x 8                    |        |       |          |
| CD74HC04E     | Texas Instruments    | Hex inverter                     |        |       |          |
| CD74HC85E     | Texas Instruments    | 4bit Logic mangitude comparator  |        |       |          |
| CD74HC125E    | Texas Instruments    | Quad tri-state buffer            |        |       |          |
| CD74HC161E    | Texas Instruments    | Counter, presetable, async reset |        |       |          |
| CD74HC173E    | Texas Instruments    | Quad D-ff, tri-state outputs     |        |       |          |
| CD74HC175E    | Texas Instruments    | Quad D-ff, compl. outputs        |        |       |          |
| CD74HC283E    | Texas Instruments    | 4bit logic Adder, Subtractor     |        |       |          |

